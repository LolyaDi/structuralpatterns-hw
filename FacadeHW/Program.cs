﻿using System;

namespace FacadeHW
{
    public class PostClient
    {
        private Courier _courier;
        private Package _package;
        private Transfer _transfer;

        public PostClient()
        {
            _courier = new Courier();
            _package = new Package();
            _transfer = new Transfer();
        }

        public string Post()
        {
            return $"{_courier.Call()}\n{_package.Pack()}\n{_transfer.Pay()}";
        }
    }

    public class Courier
    {
        public string Call()
        {
            return "Call for courier";
        }
    }

    public class Package
    {
        public string Pack()
        {
            return "Pack the package";
        }
    }

    public class Transfer
    {
        public string Pay()
        {
            return "Pay for the transfer";
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var postClient = new PostClient();

            Console.WriteLine(postClient.Post());

            Console.ReadLine();
        }
    }
}
