﻿using System;

namespace DecoratorHW
{
    public abstract class Souvenir
    {
        public abstract double GetWeight();
    }

    public class Statue : Souvenir
    {
        public override double GetWeight()
        {
            return 10.2;
        }
    }

    public class PhotoFrame : Souvenir
    {
        public override double GetWeight()
        {
            return 20.5;
        }
    }

    public abstract class Addition : Souvenir
    {
        private Souvenir _souvenir;

        public Addition(Souvenir souvenir)
        {
            _souvenir = souvenir;
        }

        public override double GetWeight()
        {
            if (_souvenir is null)
            {
                return 0;
            }
            return _souvenir.GetWeight();
        }
    }

    public class Highlight : Addition
    {
        public Highlight(Souvenir souvenir) : base(souvenir)
        {
        }

        public override double GetWeight()
        {
            return base.GetWeight() + 2.3;
        }
    }

    public class Stand : Addition
    {
        public Stand(Souvenir souvenir) : base(souvenir)
        {
        }

        public override double GetWeight()
        {
            return base.GetWeight() + 5.5;
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var statue = new Statue();
            var statueStand = new Stand(statue);

            var photoFrame = new PhotoFrame();
            var photoFrameStand = new Stand(photoFrame);

            var photoFrameHighlight = new Highlight(photoFrameStand);

            Console.WriteLine($"Statue + Stand: {statueStand.GetWeight()} kg\n" +
                                $"Photo frame + Stand: {photoFrameStand.GetWeight()} kg\n" +
                                $"Photo frame + Stand + Highlight: {photoFrameHighlight.GetWeight()} kg");

            Console.ReadLine();
        }
    }
}
